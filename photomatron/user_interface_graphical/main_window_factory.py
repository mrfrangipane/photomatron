from PySide2.QtCore import Qt
from PySide2.QtWidgets import QMainWindow, QDockWidget

from photomatron.configuration import Configuration
from photomatron.python_extensions import make_resource_path
from photomatron.user_interface_graphical.screens.welcome import WelcomeScreenWidget
from photomatron.user_interface_graphical.emulation_toolbox import EmulationToolbox


class MainWindowFactory:
    _css_editor = None  # FIXME : CSS editor will be removed when done

    @staticmethod
    def _create(welcome_screen_widget: WelcomeScreenWidget) -> QMainWindow:
        welcome_screen_widget.setFixedSize(800, 480)

        main_window = QMainWindow()
        main_window.setCentralWidget(welcome_screen_widget)

        if Configuration().show_css_editor:
            from photomatron.python_extensions.pyside.css_editor import CSSEditor
            MainWindowFactory._css_editor = CSSEditor("Photomatron", welcome_screen_widget)
        else:
            with open(make_resource_path("ui/style.css"), "r") as file_stylesheet:
                welcome_screen_widget.setStyleSheet(file_stylesheet.read())

        return main_window

    @staticmethod
    def create_for_emulated_raspberry_pi(central_widget: WelcomeScreenWidget) -> QMainWindow:
        main_window = MainWindowFactory._create(central_widget)

        main_window.setWindowTitle("Photomatron 3 (emulated)")

        toolbox_dock = QDockWidget("Emulation toolbox", main_window)
        toolbox_dock.setAllowedAreas(Qt.RightDockWidgetArea)
        toolbox_dock.setWidget(EmulationToolbox())
        main_window.addDockWidget(Qt.RightDockWidgetArea, toolbox_dock)

        return main_window

    @staticmethod
    def create_for_real_raspberry_pi(central_widget: WelcomeScreenWidget) -> QMainWindow:
        main_window = MainWindowFactory._create(central_widget)

        main_window.setWindowTitle("Photomatron 3")
        main_window.setWindowFlag(Qt.FramelessWindowHint)

        return main_window
