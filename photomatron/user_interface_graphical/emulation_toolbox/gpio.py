from PySide2.QtCore import QTimer
from PySide2.QtWidgets import QPushButton, QGridLayout, QGroupBox, QCheckBox

from photomatron.configuration import Configuration


class GPIO(QGroupBox):
    """Injects data into "emulated" implementations of components

    Knows that `Configuration().gpio` is an instance of `EmulatedGPIO`"""

    def __init__(self, parent=None):
        QGroupBox.__init__(self, parent)

        self.setTitle("GPIO")

        self._button_left = QPushButton("GPIO 38 (left)")
        self._button_left.pressed.connect(self._button_left_changed)
        self._button_left.released.connect(self._button_left_changed)

        self._button_center = QPushButton("GPIO 35 (center)")
        self._button_center.pressed.connect(self._button_center_changed)
        self._button_center.released.connect(self._button_center_changed)

        self._button_right = QPushButton("GPIO 37 (right)")
        self._button_right.pressed.connect(self._button_right_changed)
        self._button_right.released.connect(self._button_right_changed)

        self._check_led = QCheckBox("GPIO 36 (led)")
        self._check_led.setEnabled(False)

        layout = QGridLayout(self)
        layout.addWidget(self._button_left, 0, 0)
        layout.addWidget(self._button_center, 1, 0)
        layout.addWidget(self._button_right, 2, 0)
        layout.addWidget(self._check_led, 1, 1)

        self._timer_led = QTimer()
        self._timer_led.timeout.connect(self._timer_led_timeout)
        self._timer_led.start(20)

    def _button_left_changed(self):
        Configuration().gpio.pin_left_value = self._button_left.isDown()

    def _button_center_changed(self):
        Configuration().gpio.pin_center_value = self._button_center.isDown()

    def _button_right_changed(self):
        Configuration().gpio.pin_right_value = self._button_right.isDown()

    def _timer_led_timeout(self):
        self._check_led.setChecked(Configuration().gpio.pin_led_value)
