from PySide2.QtWidgets import QWidget, QGridLayout

from photomatron.user_interface_graphical.emulation_toolbox.gpio import GPIO


class EmulationToolbox(QWidget):
    """Injects data into "emulated" implementations of components"""

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        layout = QGridLayout(self)
        layout.addWidget(GPIO())

        layout.addWidget(QWidget())
        layout.setRowStretch(layout.rowCount() - 1, 100)
