from PySide2.QtWidgets import QDialog, QListWidget, QPushButton, QLineEdit, QGridLayout, QLabel, QWidget

from photomatron.configuration import Configuration
from photomatron.python_extensions.pyside import styled


class ManageWifiDialog(QDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self, parent)

        self.setWindowTitle("Manage Wifi")

        # Available
        self._list_available = styled(QListWidget(), 'small')

        self._button_join = styled(QPushButton("Join"), "small")
        self._button_join.clicked.connect(self._join)

        self._line_password = styled(QLineEdit(), 'small')

        # Known
        self._list_known = styled(QListWidget(), 'small')

        self._button_forget = styled(QPushButton("Forget"), "small")
        self._button_forget.clicked.connect(self._forget)

        # Close
        self._button_close = styled(QPushButton("Close"), "medium")
        self._button_close.clicked.connect(self.accept)

        layout = QGridLayout(self)

        layout.addWidget(styled(QLabel("Available SSIDs"), 'small'), 0, 0, 1, 3)
        layout.addWidget(self._list_available, 1, 0, 1, 3)
        layout.addWidget(styled(QLabel("Password:"), 'small'), 2, 0)
        layout.addWidget(self._line_password, 2, 1)
        layout.addWidget(self._button_join, 2, 2)

        layout.addWidget(styled(QLabel("Known SSIDs"), 'small'), 0, 3, 1, 2)
        layout.addWidget(self._list_known, 1, 3, 1, 2)
        layout.addWidget(QWidget(), 2, 3)
        layout.addWidget(self._button_forget, 2, 4)

        layout.addWidget(self._button_close, 3, 0, 1, 5)

        layout.setColumnStretch(0, 5)
        layout.setColumnStretch(1, 40)
        layout.setColumnStretch(2, 5)
        layout.setColumnStretch(3, 50)

        self._list_known.addItems(["SSID2", "FreeBritney"] * 10)
        self._list_available.addItems(["FrangiFi_5Ghz", "JeanJean", "box-CD68Q5E"])

    def _forget(self):
        pass

    def _join(self):
        pass
