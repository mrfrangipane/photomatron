from PySide2.QtCore import QTimer
from PySide2.QtWidgets import QFrame, QGridLayout, QPushButton, QLabel, QWidget

from photomatron.python_extensions.pyside import styled
from photomatron.user_interface_graphical.screens.wifi import ManageWifiDialog


class WelcomeScreenWidget(QFrame):
    def __init__(self, parent=None):
        QFrame.__init__(self, parent)

        self._timer = QTimer(self)
        self._timer.timeout.connect(self._timer_timeout)

        label_title = styled(QLabel("Photomatron 3"), style_name="title")

        self._label_status_row_1 = styled(QLabel("status row 1 and long text to show"), style_name="wifi")
        self._label_status_row_2 = styled(QLabel("status row 2"), style_name="wifi")

        self._button_launch_photobooth = styled(QPushButton("Launch Photobooth"), style_name="large")

        self._button_app_update = styled(QPushButton("Update app"), style_name="medium")
        self._button_app_exit = styled(QPushButton("Exit app"), style_name="medium")
        self._button_app_configure = styled(QPushButton("Configure app..."), style_name="medium")

        self._button_network_renew = styled(QPushButton("Renew DHCP"), style_name="medium")
        self._button_manage_wifi = styled(QPushButton("Manage Wifi..."), style_name="medium")
        self._button_manage_wifi.clicked.connect(self._manage_wifi)

        self._button_pi_shutdown = styled(QPushButton("Shutdown Pi"), style_name="medium")
        self._button_pi_reboot = styled(QPushButton("Reboot Pi"), style_name="medium")
        self._button_show_log = styled(QPushButton("Show log..."), style_name="medium")

        layout = QGridLayout(self)

        layout.addWidget(label_title, 0, 0, 2, 1)
        layout.addWidget(self._label_status_row_1, 0, 1, 1, 2)
        layout.addWidget(self._label_status_row_2, 1, 1, 1, 2)

        layout.addWidget(QWidget(), 2, 0)

        layout.addWidget(self._button_launch_photobooth, 3, 0, 1, 3)

        layout.addWidget(QWidget(), 4, 0)

        layout.addWidget(self._button_app_update, 5, 0)
        layout.addWidget(self._button_app_exit, 5, 1)
        layout.addWidget(self._button_app_configure, 5, 2)

        layout.addWidget(self._button_network_renew, 6, 0)
        layout.addWidget(self._button_manage_wifi, 6, 1)

        layout.addWidget(self._button_pi_reboot, 7, 0)
        layout.addWidget(self._button_pi_shutdown, 7, 1)
        layout.addWidget(self._button_show_log, 7, 2)

        layout.setRowStretch(2, 100)
        layout.setRowStretch(4, 100)

        self._timer.start(500)

    def _timer_timeout(self):
        pass

    def any_button_pressed(self):
        self._label_status_row_2.setText(self._label_status_row_2.text() + "a")

    def _manage_wifi(self):
        wifi_manager_dialog = ManageWifiDialog(self)
        wifi_manager_dialog.setGeometry(self.geometry())
        wifi_manager_dialog.move(self.mapToGlobal(self.pos()))
        wifi_manager_dialog.show()
