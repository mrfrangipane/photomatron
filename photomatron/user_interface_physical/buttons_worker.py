from PySide2.QtCore import QObject, Signal

from photomatron.configuration import Configuration


# FIXME : separate PySide from domain ?


class ButtonsWorker(QObject):
    """Runs in a different thread an infinite loop polling GPIO for button changes"""
    left_pressed = Signal()
    center_pressed = Signal()
    right_pressed = Signal()

    left_released = Signal()
    center_released = Signal()
    right_released = Signal()

    def __init__(self, parent=None):
        QObject.__init__(self, parent)
        self._is_running = False

    def exec(self):
        self._is_running = True

        previous_state_left = Configuration().gpio.get_pin_left_value()
        previous_state_center = Configuration().gpio.get_pin_center_value()
        previous_state_right = Configuration().gpio.get_pin_right_value()

        while self._is_running:
            state_left = Configuration().gpio.get_pin_left_value()
            state_center = Configuration().gpio.get_pin_center_value()
            state_right = Configuration().gpio.get_pin_right_value()

            if state_left != previous_state_left:
                previous_state_left = state_left

                if state_left:
                    self.left_pressed.emit()
                else:
                    self.left_released.emit()

            if state_center != previous_state_center:
                previous_state_center = state_center

                if state_center:
                    self.center_pressed.emit()
                else:
                    self.center_released.emit()

            if state_right != previous_state_right:
                previous_state_right = state_right

                if state_right:
                    self.right_pressed.emit()
                else:
                    self.left_released.emit()

    def stop(self):
        self._is_running = False
