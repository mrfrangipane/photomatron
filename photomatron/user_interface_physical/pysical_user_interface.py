from PySide2.QtCore import QThread, QObject, Signal

from photomatron.configuration import Configuration
from photomatron.user_interface_physical.buttons_worker import ButtonsWorker


class PhysicalUserInterface(QObject):
    left_pressed = Signal()
    center_pressed = Signal()
    right_pressed = Signal()

    left_released = Signal()
    center_released = Signal()
    right_released = Signal()

    def __init__(self, parent=None):
        QObject.__init__(self, parent)
        self._buttons_thread = QThread()
        self._buttons_worker = ButtonsWorker()

        self._buttons_worker.left_pressed.connect(self._pressed)
        self._buttons_worker.left_pressed.connect(self.left_pressed)
        self._buttons_worker.center_pressed.connect(self._pressed)
        self._buttons_worker.center_pressed.connect(self.center_pressed)
        self._buttons_worker.right_pressed.connect(self._pressed)
        self._buttons_worker.right_pressed.connect(self.right_pressed)

        self._buttons_worker.left_released.connect(self._released)
        self._buttons_worker.left_released.connect(self.left_released)
        self._buttons_worker.center_released.connect(self._released)
        self._buttons_worker.center_released.connect(self.center_released)
        self._buttons_worker.right_released.connect(self._released)
        self._buttons_worker.right_released.connect(self.right_released)

    def start(self):
        self._buttons_worker.moveToThread(self._buttons_thread)
        self._buttons_thread.started.connect(self._buttons_worker.exec)
        self._buttons_thread.finished.connect(self._buttons_thread.deleteLater)
        self._buttons_thread.start()

    def stop(self):
        self._buttons_worker.stop()  # why using Signal is not working ?
        self._buttons_thread.quit()

    def _pressed(self):
        Configuration().gpio.set_pin_led(True)

    def _released(self):
        Configuration().gpio.set_pin_led(False)
