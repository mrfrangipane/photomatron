from photomatron.gpio.abstract import AbstractGPIO


class EmulatedGPIO(AbstractGPIO):

    def __init__(self):
        self.pin_left_value = False
        self.pin_center_value = False
        self.pin_right_value = False
        self.pin_led_value = False

    def get_pin_left_value(self):
        return self.pin_left_value

    def get_pin_center_value(self):
        return self.pin_center_value

    def get_pin_right_value(self):
        return self.pin_right_value

    def set_pin_led(self, value):
        self.pin_led_value = value
