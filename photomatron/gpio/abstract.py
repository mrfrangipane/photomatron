from abc import ABC, abstractmethod


class AbstractGPIO(ABC):

    @abstractmethod
    def get_pin_left_value(self) -> bool:
        pass

    @abstractmethod
    def get_pin_center_value(self):
        pass

    @abstractmethod
    def get_pin_right_value(self):
        pass

    @abstractmethod
    def set_pin_led(self, value):
        pass