import logging
import sys

from PySide2.QtWidgets import QApplication

from photomatron.configuration import Configuration
from photomatron.configurator.argument_parser import ArgumentParserConfigurator

from photomatron.user_interface_graphical.screens.welcome import WelcomeScreenWidget
from photomatron.user_interface_graphical.main_window_factory import MainWindowFactory
from photomatron.user_interface_physical import PhysicalUserInterface


def configure_and_run():
    configurator = ArgumentParserConfigurator()
    configurator.load_configuration()

    if configurator.is_raspberry_emulated:
        from photomatron.gpio.emulated import EmulatedGPIO
        Configuration().gpio = EmulatedGPIO()

    else:
        from photomatron.gpio.real import RealGPIO
        Configuration().gpio = RealGPIO()

    Configuration().working_folder = configurator.working_folder
    Configuration().show_css_editor = configurator.show_css_editor

    application = QApplication()

    welcome_screen_widget = WelcomeScreenWidget()

    if configurator.is_raspberry_emulated:
        main_window = MainWindowFactory.create_for_emulated_raspberry_pi(welcome_screen_widget)
        main_window.show()
    else:
        main_window = MainWindowFactory.create_for_real_raspberry_pi(welcome_screen_widget)
        main_window.showMaximized()

    physical_user_interface = PhysicalUserInterface()

    application.aboutToQuit.connect(physical_user_interface.stop)
    physical_user_interface.left_pressed.connect(welcome_screen_widget.any_button_pressed)
    physical_user_interface.center_pressed.connect(welcome_screen_widget.any_button_pressed)
    physical_user_interface.right_pressed.connect(welcome_screen_widget.any_button_pressed)

    physical_user_interface.start()

    return application.exec_()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    sys.exit(configure_and_run())
