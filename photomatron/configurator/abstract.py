from abc import ABC, abstractmethod


class AbstractConfigurator(ABC):
    """Holds values that will be used to configure application singleton
    `photomatron.application.configuration.Configuration()`"""
    def __init__(self):
        self.working_folder: str = None
        self.show_css_editor: bool = False
        self.is_raspberry_emulated: bool = False

    @abstractmethod
    def load_configuration(self):
        """Must fill class members"""
        pass
