import argparse

from photomatron.configurator.abstract import AbstractConfigurator


class ArgumentParserConfigurator(AbstractConfigurator):
    def load_configuration(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--working-folder', '-f', required=True)
        parser.add_argument('--show-css-editor', '-c', action='store_true')
        parser.add_argument('--emulated-raspberry', '-e', action='store_true')
        args = parser.parse_args()

        self.working_folder = args.working_folder
        self.show_css_editor = args.show_css_editor
        self.is_raspberry_emulated = args.emulated_raspberry
