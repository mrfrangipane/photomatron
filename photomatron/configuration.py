from dataclasses import dataclass

from photomatron.python_extensions.singleton_metaclass import SingletonMetaclass
from photomatron.gpio.abstract import AbstractGPIO


@dataclass
class Configuration(metaclass=SingletonMetaclass):
    """Singleton holding application classes and info that have been configured at startup"""
    working_folder: str = None
    gpio: AbstractGPIO = None
    show_css_editor: bool = None
