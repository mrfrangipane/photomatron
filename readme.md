# Photomatron 3

** Moved to https://github.com/MrFrangipane/photobooth-legacy**

Third rewrite of a Photobooth for Raspberry Pi 3+

## Install

```
sudo apt install python3-pyside2.qt3dcore python3-pyside2.qt3dinput python3-pyside2.qt3dlogic python3-pyside2.qt3drender python3-pyside2.qtcharts python3-pyside2.qtconcurrent python3-pyside2.qtcore python3-pyside2.qtgui python3-pyside2.qthelp python3-pyside2.qtlocation python3-pyside2.qtmultimedia python3-pyside2.qtmultimediawidgets python3-pyside2.qtnetwork python3-pyside2.qtopengl python3-pyside2.qtpositioning python3-pyside2.qtprintsupport python3-pyside2.qtqml python3-pyside2.qtquick python3-pyside2.qtquickwidgets python3-pyside2.qtscript python3-pyside2.qtscripttools python3-pyside2.qtsensors python3-pyside2.qtsql python3-pyside2.qtsvg python3-pyside2.qttest python3-pyside2.qttexttospeech python3-pyside2.qtuitools python3-pyside2.qtwebchannel python3-pyside2.qtwebsockets python3-pyside2.qtwidgets python3-pyside2.qtx11extras python3-pyside2.qtxml python3-pyside2.qtxmlpatterns python3-pyside2uic
/usr/bin/python3 -m pip install git+https://github.com/cube-creative/guibedos.git
/usr/bin/python3 -m pip install git+https://gitlab.com/mrfrangipane/photomatron.git
```

Don't forget to install the printer within cups

Don't forget to install the typo Coolvetica (usually in `~/.fonts`)

## Auto launch

- edit or create the file `~/.config/lxsession/LXDE-pi/autostart`
- fill it with

```
/usr/bin/python3 -m photomatron -f /home/pi/photobooth
@lxterminal
```

### Check logs

_Only `stdout` will show_ 

```
cat ~/.cache/lxsession/LXDE-pi/run.log 
```

### Notes

Sample image provided by [ykaiavu](https://pixabay.com//users/ykaiavu-7068951)
